[![Build status][appveyor-badge]][appveyor-badge-url]

# Ember-bootstrap

This README outlines the details of collaborating on this Ember addon.

## Installation

* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).

[appveyor-badge]: https://ci.appveyor.com/api/projects/status/hyjmh9a2u6qvkv9y?svg=true
[appveyor-badge-url]: https://ci.appveyor.com/project/klinem/ember-bootstrap
