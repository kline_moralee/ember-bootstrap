import Ember from 'ember';

const { computed } = Ember;

export default Ember.Mixin.create({
  /**
   * @property variant
   * @type String
   * @default null
   */
  variant: null,

  /**
   * @property variantPrefix
   * @type String
   * @default null
   */
  variantPrefix: null,

  /**
   * @property variantSuffix
   * @type String
   * @default null
   */
  variantSuffix: null,

  /**
   * @property variantClass (computed)
   * @type String
   * @default null
   */
  variantClass: computed('variant', function () {
    const { variantPrefix, variantSuffix, variant } = this;

    if (!variant) {
      return null;
    }

    let className = variant;

    if (variantPrefix) {
      className = `${variantPrefix}-${className}`;
    }

    if (variantSuffix) {
      className = `${className}-${variantSuffix}`;
    }

    return className;
  })
});
