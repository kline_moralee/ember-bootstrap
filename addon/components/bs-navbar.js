import Ember from 'ember';

const { computed } = Ember;

export default Ember.Component.extend({
  tagName: 'nav',

  classNames: ['navbar'],

  classNameBindings: [
    'full:navbar-full',
    'variantClass',
    'fixedClass'
  ],

  /**
   * Determines if the navbar has the `navbar-full` class.
   *
   * @property full
   * @type Boolean
   * @default false
   */
  full: false,

  /**
   * Determines the navbar's variant class (eg: navbar-light, navbar-dark)
   *
   * @property variant
   * @type String
   * @default 'light'
   */
  variant: 'light',

  /**
   * Determines the navbar's fixed class (eg: navbar-fixed-top, navbar-fixed-bottom)
   *
   * @property fixed
   * @type String
   * @default null
   */
  fixed: null,

  variantClass: computed('variant', function () {
    if (!this.variant) {
      return null;
    }

    return `navbar-${this.variant}`;
  }),

  fixedClass: computed('fixed', function () {
    if (!this.fixed) {
      return null;
    }

    return `navbar-fixed-${this.fixed}`;
  })
});
