import Ember from 'ember';

const { computed } = Ember;

export default Ember.Component.extend({
  tagName: 'ul',

  classNames: ['nav'],

  classNameBindings: [
    'stacked:nav-stacked',
    'variantClass'
  ],

  /**
   * @property stacked
   * @type Boolean
   * @default false
   */
  stacked: false,

  /**
   * @property variant
   * @type String
   * @default null
   */
  variant: null,

  variantClass: computed('variant', function () {
    if (!this.variant) {
      return null;
    }

    if (this.variant === 'navbar') {
      return 'navbar-nav';
    }

    return `nav-${this.variant}`;
  })
});
