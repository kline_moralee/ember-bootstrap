import Ember from 'ember';
import VariantClass from 'ember-bootstrap/mixins/variant-class';

// const { computed } = Ember;

export default Ember.Component.extend(VariantClass, {
  tagName: 'button',

  classNames: ['btn'],

  classNameBindings: [
    'variantClass'
  ],

  attributeBindings: [
    'type'
  ],

  variantPrefix: 'btn',

  /**
   * @property type
   * @type String
   * @default button
   */
  type: 'button'
});
