/* jshint expr:true */
import { expect } from 'chai';
import {
  describeComponent,
  it
} from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

const classes = {
  'main': 'btn'
};

describeComponent(
  'bs-button',
  'Integration: BsButtonComponent',
  {
    integration: true
  },
  function() {
    it('should render', function() {
      this.render(hbs`{{bs-button}}`);

      const button = this.$('button');

      expect(button).to.have.length(1);
      expect(button.hasClass(classes.main)).to.be.true;
      expect(button.attr('type')).to.equal('button');
    });

    it('should add the `variant` class', function () {
      this.render(hbs`{{bs-button variant="primary"}}`);

      expect(this.$('button').hasClass(`${classes.main}-primary`)).to.be.true;
    });
  }
);
