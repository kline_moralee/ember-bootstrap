/* jshint expr:true */
import { expect } from 'chai';
import {
  describeComponent,
  it
} from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

const classes = {
  'main': 'navbar',
  'full': 'navbar-full',
  'fixedTop': 'navbar-fixed-top',
  'fixedBottom': 'navbar-fixed-bottom',
  'light': 'navbar-light',
  'dark': 'navbar-dark'
};

describeComponent(
  'bs-navbar',
  'Integration: BsNavbarComponent',
  {
    integration: true
  },
  function() {
    it('should render', function() {
      this.render(hbs`{{bs-navbar}}`);

      const component = this.$('nav');

      expect(component).to.have.length(1);
      expect(component.hasClass(classes.main)).to.be.true;
      expect(component.hasClass(classes.light)).to.be.true;
      expect(component.hasClass(classes.fixedTop)).to.be.false;
      expect(component.hasClass(classes.fixedBottom)).to.be.false;
    });

    it('should add the `full` class', function () {
      this.render(hbs`{{bs-navbar full=true}}`);

      expect(this.$('nav').hasClass(classes.full)).to.be.true;
    });

    it('should add the `fixed` class', function () {
      this.render(hbs`{{bs-navbar fixed="top"}}`);

      expect(this.$('nav').hasClass(classes.fixedTop)).to.be.true;

      this.render(hbs`{{bs-navbar fixed="bottom"}}`);

      expect(this.$('nav').hasClass(classes.fixedBottom)).to.be.true;
    });

    it('should add the `variant` class', function () {
      this.render(hbs`{{bs-navbar variant="dark"}}`);

      expect(this.$('nav').hasClass(classes.dark)).to.be.true;
    });
  }
);
