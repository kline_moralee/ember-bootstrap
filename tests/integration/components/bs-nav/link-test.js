/* jshint expr:true */
import { expect } from 'chai';
import {
  describeComponent,
  it
} from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

const classes = {
  'main': 'nav-link'
};

describeComponent(
  'bs-nav/link',
  'Integration: BsNavLinkComponent',
  {
    integration: true
  },
  function() {
    it('should render', function() {
      this.render(hbs`{{bs-nav/link}}`);

      const component = this.$('a');

      expect(component).to.have.length(1);
      expect(component.hasClass(classes.main)).to.be.true;
    });
  }
);
