/* jshint expr:true */
import { expect } from 'chai';
import {
  describeComponent,
  it
} from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

const classes = {
  'main': 'nav-item'
};

describeComponent(
  'bs-nav/item',
  'Integration: BsNavItemComponent',
  {
    integration: true
  },
  function() {
    it('should render', function() {
      this.render(hbs`{{bs-nav/item}}`);

      const component = this.$('li');

      expect(component).to.have.length(1);
      expect(component.hasClass(classes.main)).to.be.true;
    });
  }
);
