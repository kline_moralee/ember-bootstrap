/* jshint expr:true */
import { expect } from 'chai';
import {
  describeComponent,
  it
} from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

const classes = {
  'main': 'nav',
  'tabs': 'nav-tabs',
  'pills': 'nav-pills',
  'navbar': 'navbar-nav',
  'stacked': 'nav-stacked'
};

describeComponent(
  'bs-nav',
  'Integration: BsNavComponent',
  {
    integration: true
  },
  function() {
    it('should render', function() {
      this.render(hbs`{{bs-nav}}`);

      const component = this.$('ul');

      expect(component).to.have.length(1);
      expect(component.hasClass(classes.main)).to.be.true;
    });

    it('should add the `variant` class', function () {
      this.render(hbs`{{bs-nav variant="tabs"}}`);
      expect(this.$('ul').hasClass(classes.tabs)).to.equal(true,
        `should have '${classes.tabs}' class`);

      this.render(hbs`{{bs-nav variant="pills"}}`);
      expect(this.$('ul').hasClass(classes.pills)).to.equal(true,
        `should have '${classes.pills}' class`);

      this.render(hbs`{{bs-nav variant="navbar"}}`);
      expect(this.$('ul').hasClass(classes.navbar)).to.equal(true,
        `should have '${classes.navbar}' class`);
    });

    it('should add the `stacked` class', function () {
      this.render(hbs`{{bs-nav stacked=true}}`);
      expect(this.$('ul').hasClass(classes.stacked)).to.be.true;
    });
  }
);
