import Ember from 'ember';

export default Ember.Controller.extend({
  navLinks: {
    'comps.buttons': 'Buttons',
    'comps.dropdowns': 'Dropdowns',
    'comps.navs': 'Navs',
    'comps.navbar': 'Navbar'
  }
});
