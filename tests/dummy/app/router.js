import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('comps', { path: 'components' }, function () {
    this.route('buttons');
    this.route('dropdowns');
    this.route('navs');
    this.route('navbar');
  });
});

export default Router;
